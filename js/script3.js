/*Aceitar apenas telefones válidos
Aceitar apenas e-mail válidos

preço base da diária: 170

 */
$(function(){
    inicializarFormulario();
    $('#checkin').change(function(){
        //toda vez que o checkin muda de valor
        mudarCheckOut();
    })
})
function inicializarFormulario(){
    var hoje = formataData(new Date());
    $('#checkin').val(hoje);//inicializo o checkin com a data atual
    $('#checkin').attr('min', hoje);
    mudarCheckOut();
}
function mudarCheckOut(){
        var mesCheckin = new Date($('#checkin').val() + " 00:00:00").getMonth() + 1;
        var minDiarias = (mesCheckin == 1 || mesCheckin == 7) ? 4 : 1;
        var novaDataCheckout = somaDiasData($('#checkin').val(), minDiarias);//Inicializa o checkout com a data de checkin + 1 dia
        $('#checkout').val(novaDataCheckout);
        $('#checkout').attr('min', novaDataCheckout);

}

function somaDiasData(dataAtual, dias){
    var dataSaida = new Date(dataAtual + " 00:00:00")
    dataSaida.setDate(dataSaida.getDate() + dias);
    return formataData(dataSaida)
}

function formataData(data){
    var dia, mes, ano, dataRetorno;
    dia = data.getDate();
    mes = data.getMonth()+1;
    ano = data.getFullYear();
    dia = (dia < 10) ? '0' + dia : dia;
    mes = (mes < 10) ? '0' + mes : mes;
    dataRetorno = ano + '-' + mes + '-' + dia;
    return dataRetorno;
}
