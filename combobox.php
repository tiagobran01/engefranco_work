<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Engefranco</title>
  <!-- <link rel="stylesheet" href="css/hover.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@1,700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Oswald|Roboto&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="css/swiper.min.css"> -->
  <link rel="stylesheet" href="css/style.css">
    <?php
        $conexao =new PDO ("mysql:host =localhost;dbname=ce","root","");

        $conexao->exec('SET CHARACTER SET utf8');
    ?>
</head>
<body>

    <select name="estados" id="estados">
        <?php
            $select = $conexao ->prepare("SELECT * FROM estados ORDER BY nome ASC");
            $select -> execute();
            $fetchAll=$select->fetchAll();
            foreach($fetchAll as $estados)  
            {
                echo '<option value="'.$estados['id'].'">'.$estados['nome'].'</option>';
            }
        ?>
        </select>
    <select id="cidades" style="display:none"></select>

  <script src="js/jquery.min.js"></script>
  <!-- <script src="js/bootstrap.min.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.12/js/all.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <script src="js/swiper.min.js"></script>
  <script src="js/jquery.counterup.min.js"></script>
  <script src="js/countUp.min.js"></script> -->
  <script>
      $.("#estados").on("change", function(){
        
            var idEstado = $("#estados").val();

        $.ajax({
            url:'pega_cidades.php',
            type:'POST',
            data:{id:idEstado},
            beforeSend:function(){
                $("#cidades").css({'display':'block'});
                $("#cidades").html("Carregando...");
            },
            success: function(data){
                $("#cidades").css({'display':'block'});
                $("#cidades").html(data);
            },
            error: function(data){
                $("#cidades").css({'display':'block'});
                $("#cidades").html("Houve um erro ao carregar");
            }   
        });
      });
      

  </script>
  <!-- <script>
    $(document).ready(function(){
      $('.counter').counterUp({
        delay: 10,
        time: 1000
      });
    })
      // $(document).ready(function() {
      //   $('select').select2();
      // })
  </script> -->
  <!-- Initialize Swiper -->
<!-- <script>
  var swiper = new Swiper('.swiper-container', {
    pagination: {
      el: '.swiper-pagination',
      type: 'progressbar',
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
</script> -->
</body>
</html>